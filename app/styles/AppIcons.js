import React, {useEffect} from 'react';
import {Text} from 'react-native';
import PropTypes from 'prop-types';

import {default as Entypo} from 'react-native-vector-icons/dist/Entypo';
import {default as AntDesign} from 'react-native-vector-icons/dist/AntDesign';
import {default as Feather} from 'react-native-vector-icons/dist/Feather';
import {default as Ionicons} from 'react-native-vector-icons/dist/Ionicons';
import {default as FontAwesome} from 'react-native-vector-icons/dist/FontAwesome';
import {default as FontAwesome5} from 'react-native-vector-icons/dist/FontAwesome5';
import {default as MaterialIcons} from 'react-native-vector-icons/dist/MaterialIcons';
import {default as MaterialCommunityIcons} from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {default as Fontisto} from 'react-native-vector-icons/dist/Fontisto';
import {default as Octicons} from 'react-native-vector-icons/dist/Octicons';
import {default as SimpleLineIcons} from 'react-native-vector-icons/dist/SimpleLineIcons';
import {default as FontAwesome5_Regular} from 'react-native-vector-icons/dist/FontAwesome5';
import {default as FontAwesome5_Solid} from 'react-native-vector-icons/dist/FontAwesome5';
import {default as Foundation} from 'react-native-vector-icons/dist/Foundation';
import {default as Zocial} from 'react-native-vector-icons/dist/Zocial';
import {default as EvilIcons} from 'react-native-vector-icons/dist/EvilIcons';


const iconFamilies = [
  {name: 'Zocial', component: Zocial},
  {name: 'Entypo', component: Entypo},
  {name: 'Feather', component: Feather},
  {name: 'Ionicons', component: Ionicons},
  {name: 'Fontisto', component: Fontisto},
  {name: 'Octicons', component: Octicons},
  {name: 'EvilIcons', component: EvilIcons},
  {name: 'AntDesign', component: AntDesign},
  {name: 'Foundation', component: Foundation},
  {name: 'FontAwesome', component: FontAwesome},
  {name: 'FontAwesome5', component: FontAwesome5},
  {name: 'MaterialIcons', component: MaterialIcons},
  {name: 'MaterialCommunityIcons', component: MaterialCommunityIcons},
  {name: 'SimpleLineIcons', component: SimpleLineIcons},
  {name: 'FontAwesome5_Regular', component: FontAwesome5_Regular},
  {name: 'FontAwesome5_Solid', component: FontAwesome5_Solid},
];
/**
 * @param {AppIcons.propTypes} props
 * @description  Choose Icon from  React Native Vector Icon
 */

const AppIcons = (props) =>
  iconFamilies.map(
    (item, index) => item.name === props.type && <item.component {...props}  key={index}/>,
  );

AppIcons.propTypes= {
    /**Icon type */
  type: PropTypes.string.isRequired,
      /**Icon name */
  name: PropTypes.string.isRequired,
      /**Icon color */
  color: PropTypes.string,
      /**Icon number */
  size: PropTypes.number,
      /**Icon event */
  onPress: PropTypes.func,
      /**Icon style */
  style: PropTypes.object,
};

export default AppIcons;

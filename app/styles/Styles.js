import {StyleSheet, Dimensions, Platform, I18nManager} from 'react-native';
import Colors from './Colors';

const AppStyles = StyleSheet.create({
  text: {
    fontFamily: I18nManager.isRTL ? 'Cairo-Regular' : 'Roboto',
  },
  boldText: {
    fontFamily: I18nManager.isRTL ? 'Cairo-Regular' : 'Roboto-Bold',
  },
  input: {
    width: '90%',
    paddingVertical: 3,
    borderRadius: 10,
    alignSelf: 'center',
    color: '#000',
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 3,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '2%',
    backgroundColor: Colors.header,
    paddingVertical: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
});
export default AppStyles;

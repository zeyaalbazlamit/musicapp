import React, {useState, useEffect} from 'react';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import {
  StyleSheet,
  Text,
  ScrollView,
  Dimensions,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  View,
  Image,
  Pressable,
  I18nManager,
} from 'react-native';

import {AppStyles, AppIcons} from '@styles';
import {useSelector, useDispatch} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
// import {SAVEUSERINFO} from '../store/actions/userProfile';

function Login({navigation}) {
  const dispatch = useDispatch();
  signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      setloggedIn(false);
      setuserInfo([]);
    } catch (error) {
      console.error(error);
    }
  };

  const googleLogin = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();

      console.log(userInfo);
      // setloggedIn(true);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    GoogleSignin.configure({
      scopes: ['profile', 'email'],
      // webClientId:
      //   '451004924816-o0qeq5onpofrvg480v2bb9f74tri443l.apps.googleusercontent.com',
      // offlineAccess: true,-
    });
  }, []);
  return (
    <View
      style={{
        alignItems: 'center',
        width: '100%',
        height: '100%',
      }}>
      <TouchableOpacity
        onPress={googleLogin}
        style={{
          ...Styles.socialButton,
          backgroundColor: '#db4437',
          marginTop: 20,
        }}>
        <AppIcons
          name="google"
          type="Fontisto"
          color="#fff"
          style={{marginVertical: 5}}
        />

        <Text
          style={[
            {
              color: '#fff',
              fontSize: 12,
              paddingVertical: 4,
              marginHorizontal: 5,
            },
          ]}>
          google
        </Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => navigation.navigate('BottomTabs')}>
        <Text style={{color: '#000'}}>GO TO NEXT SCREEN </Text>
      </TouchableOpacity>
    </View>
  );
}

const Styles = StyleSheet.create({
  input: {
    margin: 10,
    backgroundColor: 'grey',
    opacity: 0.7,
    // padding: 15,
    paddingHorizontal: 10,
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 12,
    width: '90%',
    fontWeight: 'bold',
  },
  btn: {
    borderRadius: 20,
    minWidth: '60%',
    paddingHorizontal: 10,
    paddingVertical: 7,
    marginVertical: 15,
    backgroundColor: '#DB9A11',
  },
  txt: {
    color: 'white',
    fontWeight: 'bold',
    alignItems: 'center',
    fontSize: 20,
    textAlign: 'center',
  },
  socialButton: {
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 2,
    minWidth: '90%',
    alignItems: 'center',
    alignSelf: 'center',
    paddingVertical: 5,
    flexDirection: 'row',
    marginVertical: 10,
  },
});

export default Login;

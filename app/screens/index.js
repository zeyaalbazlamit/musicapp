import Lyrics from './Lyrics';
import ArtAlbums from './ArtAlbums';
import LogIn from './LogIn';
import Artists from './Artists';
import Albums from './Albums';
import Search from './Search';
import Tracks from './Tracks';
import Splash from './Splash';


export {Lyrics, ArtAlbums, LogIn, Artists,Splash, Albums, Search,Tracks};

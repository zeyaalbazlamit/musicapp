import React, {useState, useEffect} from 'react';
import {Text, View, FlatList, StyleSheet, Pressable} from 'react-native';
import axios from 'axios';
import FastImage from 'react-native-fast-image';
import {AppIcons} from '@styles';
import {Header} from '@components';
import {apikey, API} from '@services';

function ArtAlbums({navigation, route}) {
  const [data, setData] = useState([]);
  const {artID} = route.params;
  const goBack = () => navigation.goBack();

  const getArtAlbums = () => {
    let URL = `${API}/artists/${artID}/albums`;
    // console.log('-----------------albums-----------------------------');
    console.log(URL);
    axios
      .get(URL, {
        params: {
          apikey,
        },
      })
      .then(res => {
        if (res.data.success == 1) {
          // console.log(res.data);
          setData(res.data.result.albums);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  useEffect(() => {
    getArtAlbums();
  }, []);

  return (
    <View>
      <Header back title="ArtAlbums" goBack={goBack} />

      <FlatList
        style={{paddingVertical: 5}}
        data={data}
        contentContainerStyle={{paddingBottom: 80}}
        keyExtractor={(item, index) => index}
        renderItem={({item}) => {
          return (
            <Pressable
              onPress={() =>
                navigation.navigate('Tracks', {
                  album: item.id_album,
                  artist: artID,
                  // track: item.id_track,
                })
              }
              style={styles.artAlbumsCard}>
              <FastImage
                style={{width: 100, height: 100, borderRadius: 10}}
                source={{uri: item.cover}}
              />
              <Text
                bold
                style={{
                  fontSize: 15,
                  flex: 1,
                  marginHorizontal: '3%',
                  color: '#000',
                }}>
                {item.album.length > 100
                  ? item.album?.substr(0, 100) + '...'
                  : item.album}
              </Text>

              <AppIcons
                name="right"
                type="AntDesign"
                color="gray"
                size={20}
                // onPress={toggleModal}
              />
            </Pressable>
          );
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  artAlbumsCard: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: '2%',
    marginHorizontal: '2%',
    borderRadius: 10,
    backgroundColor: '#fff',
    paddingVertical: 7,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 2,
  },
});

export default ArtAlbums;

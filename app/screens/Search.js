import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  Button,
  TextInput,
  FlatList,
  Pressable,
  StyleSheet,
  Dimensions,
  ImageBackground,
} from 'react-native';
import axios from 'axios';
import Carousel from 'react-native-snap-carousel';
import FastImage from 'react-native-fast-image';
import { AppIcons, AppStyles } from '@styles';
import { apikey, API } from '@services';

export default Search = ({ navigation }) => {
  const [search, setSearch] = useState([]);
  const [text, setText] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [newAlbums, setNewAlbums] = useState([]);

  const renderItem = ({ item, index }) => {
    return (
      <Pressable onPress={() =>
        navigation.navigate('ArtAlbums', {
          artID: 116785,
        })
      }>
        <ImageBackground source={{ uri: item.cover }} style={styles.slide}>
          <Text
            style={
              (styles.title, { color: '#fff', fontSize: 12, fontWeight: 'bold' })
            }>
            {item.album}
          </Text>
        </ImageBackground>
      </Pressable>
    );
  };
  const musicSearch = searchText => {
    axios
      .get(API, {
        params: {
          q: searchText,
          limit: 60,
          lyrics: 1,
          apikey,
        },
      })
      .then(res => {
        if (res.data.success == 1) {
          setSearch(res.data.result);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const handleSearchTextChange = t => {
    setText(t);
    musicSearch(t);
  };

  const getNewAlbums = () => {
    axios
      .get(API + '/artists/27856/albums', {
        params: {
          apikey,
        },
      })
      .then(res => {
        if (res.data.success == 1) {
        }
        setNewAlbums(res.data.result.albums);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    musicSearch("te")
    getNewAlbums();
  }, []);
  const NewAlbums = () => {
    return (
      <View >
        <Text style={styles.title}>New Albums</Text>
        <Carousel
          firstItem={1}
          loop
          autoplay
          style={{ marginVertical: 20 }}
          data={newAlbums}
          renderItem={renderItem}
          sliderWidth={Dimensions.get('screen').width}
          itemWidth={130}
        />
      </View>
    )

  }
  return (
    <View style={{ paddingTop: 20 }}>
      <Text style={styles.title}>Find Your best Music</Text>
      <View style={styles.searchInput}>
        <AppIcons type="FontAwesome" name="search" size={18} color="#000" />
        <TextInput
          placeholderTextColor="#c1c1c1"
          style={AppStyles.input}
          onChangeText={handleSearchTextChange}
          value={text}
          title="find your faviote song"
          placeholder="Search ..."
        />
      </View>

      <FlatList
        data={search}
        ListHeaderComponent={NewAlbums}
        contentContainerStyle={{ paddingBottom: 300 }}
        style={{ marginHorizontal: '1%', marginTop: 5 }}
        onEndReached={() => isLoading && increasePage()}
        onEndThreshold={0.5}
        keyExtractor={(item, index) => index}
        renderItem={({ item }) => {
          return (
            <Pressable
              onPress={() => {
                navigation.navigate('Lyrics', {
                  artist: item.id_artist,
                  album: item.id_album,
                  track: item.id_track,
                  image: item.cover
                });
              }}
              style={styles.artAlbumsCard}>
              <FastImage
                source={{ uri: item.cover }}
                style={{ width: 100, height: 100, borderRadius: 10 }}
              />
              <View>
                <Text
                  style={{
                    color: '#000',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  {` Song: ${item.track.length > 15
                      ? item.track?.substr(0, 15) + '...'
                      : item.track
                    }`}{' '}
                </Text>
                <Text
                  style={{ color: 'gray', fontSize: 12, textAlign: 'center' }}>
                  {` Artist: ${item.artist.length > 15
                      ? item.artist?.substr(0, 15) + '...'
                      : item.artist
                    }`}{' '}
                </Text>
                <Text
                  style={{ color: 'gray', fontSize: 12, textAlign: 'center' }}>
                  {` Album: ${item.album.length > 15
                      ? item.album?.substr(0, 15) + '...'
                      : item.album
                    }`}{' '}
                </Text>
              </View>

              <AppIcons name="right" type="AntDesign" color="gray" size={20} />
            </Pressable>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  artAlbumsCard: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: '2%',
    marginHorizontal: '2%',
    borderRadius: 10,
    backgroundColor: '#fff',
    paddingVertical: 7,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 2,
  },
  searchInput: {
    flexDirection: 'row',
    width: '95%',
    alignSelf: 'center',
    shadowColor: '#000',
    borderRadius: 10,
    alignItems: 'center',
    paddingHorizontal: 10,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.2,
    shadowRadius: 2.49,
    elevation: 5,
    marginVertical: 5,
    backgroundColor: '#fff',
    color: '#000',
  },
  slide: {
    backgroundColor: '#000',
    height: 120,
    borderRadius: 15,
    justifyContent: 'flex-end',
    paddingBottom: 3,
    paddingLeft: 5,
    overflow: 'hidden',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 3,
    marginLeft: '4%',
    color: '#000',
  },
  albumCard: {},
});

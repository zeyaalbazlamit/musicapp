import React, {useState, useEffect} from 'react';
import {Text, View, FlatList, StyleSheet, Pressable} from 'react-native';
import FastImage from 'react-native-fast-image';
import axios from 'axios';
import {Header} from '@components';
import {apikey, API} from '@services';

export default Albums = ({navigation}) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const increasePage = () => {
    setIsLoading(false);
    setPageNumber(prev => prev + 1);
  };
  const goBack = () => navigation.goBack();

  const getAlbums = () => {
    axios
      .get(API + '/artists/116785/albums', {
        params: {
          apikey,
        },
      })
      .then(res => {
        if (res.data.success == 1) {
        }
        setData(res.data.result.albums);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    getAlbums();
  }, []);

  return (
    <View>
      <Header  title={'Albums'}/>
      <FlatList
        data={data}
        numColumns={2}
        contentContainerStyle={{paddingBottom: 80}}
        style={{marginHorizontal: '2%', paddingTop: 10}}
        onEndReached={() => isLoading && increasePage()}
        onEndThreshold={0.5}
        keyExtractor={(item, index) => index}
        renderItem={({item}) => {
          return (
            <Pressable
              onPress={() =>
                navigation.navigate('ArtAlbums', {
                  artID: 116785,
                })
              }
              style={styles.card}>
              <FastImage source={{uri: item.cover}} style={{height: 200}} />
              <Text
                style={{
                  marginVertical: 4,
                  textAlign: 'center',
                  color: '#000',
                }}>
                {item.album.length > 20
                  ? item.album?.substr(0, 20) + '...'
                  : item.album}
              </Text>
            </Pressable>
          );
        }}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  card: {
    borderRadius: 10,
    marginHorizontal: '1%',
    marginBottom: 10,
    width: '50%',
    overflow: 'hidden',
    backgroundColor: '#fff',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 3,
  },
});

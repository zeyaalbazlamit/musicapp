import React, {useEffect, useRef} from 'react';
import {
  View,
  Image,
  Dimensions,
  Text,
  Animated,
  StyleSheet,
} from 'react-native';
import LottieView from 'lottie-react-native';

const Splash = ({navigation}) => {
  const {height, width} = Dimensions.get('screen');

  useEffect(() => {
    setTimeout(() => {
      navigation?.navigate('BottomTabs');
    }, 3000);
  }, []);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        //  backgroundColor:"#000"
      }}>
      <LottieView
        style={{width: width / 2, height: height / 2, alignSelf: 'center'}}
        source={require('./Music.json')}
        autoPlay
        loop
      />
    </View>
  );
};
const Styles = StyleSheet.create({});
export default Splash;

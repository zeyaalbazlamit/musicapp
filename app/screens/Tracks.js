import React, {useState, useEffect} from 'react';
import {Text, View, FlatList, Pressable, StyleSheet} from 'react-native';
import axios from 'axios';
import {Header, TrackSkeleton} from '@components';
import FastImage from 'react-native-fast-image';
import {AppIcons} from '@styles';
import {apikey, API} from '@services';
export default Tracks = ({navigation, route: {params}}) => {
  const [tracks, setTracks] = useState([]);
  const [globalData, setGlobalData] = useState({});
  const goBack = () => navigation.goBack();
  const [isLoading, setIsLoading] = useState(false);

  const increasePage = () => {
    setIsLoading(false);
    setPageNumber(prev => prev + 1);
  };
  const getTracks = () => {
    let URL = '';
    // if (params?.album && params?.artist)
    //   URL = `https://api.happi.dev/v1/music/artist/${params.artist}/albums/${params.album}/tracks`;
    // else
    URL = API + '/artists/2272/albums/44872/tracks';
    axios
      .get(URL, {
        params: {
          apikey,
        },
      })
      .then(res => {
        let {tracks, ...tracksData} = res.data.result;
        setTracks(tracks);
        setGlobalData(tracksData);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  useEffect(() => {
    getTracks();
  }, []);

  return (
    <View>
      <Header back title={'Tracks'} goBack={goBack} />
      {tracks.length >0 ? (
        <FlatList
          data={tracks}
          contentContainerStyle={{paddingBottom: 80}}
          style={{marginHorizontal: '1%', paddingTop: 10}}
          onEndReached={() => isLoading && increasePage()}
          onEndThreshold={0.5}
          keyExtractor={(item, index) => index}
          renderItem={({item}) => {
            return (
              <Pressable
                onPress={() => {
                  navigation.navigate('Lyrics', {
                    artist: globalData.id_artist,
                    album: globalData.id_album,
                    track: item.id_track,
                    image:globalData.cover
                  });
                }}
                style={styles.artAlbumsCard}>
                <FastImage
                  source={{uri: globalData.cover}}
                  style={{width: 100, height: 100, borderRadius: 10}}
                />
                <View>
                  <Text style={styles.titleText}>
                    {` Song: ${
                      item.track.length > 15
                        ? item.track?.substr(0, 15) + '...'
                        : item.track
                    }`}{' '}
                  </Text>
                  <Text style={styles.text}>
                    {` Artist: ${globalData.artist}`}{' '}
                  </Text>
                  <Text style={styles.text}>
                    {` Album: ${globalData.album}`}{' '}
                  </Text>
                </View>

                <AppIcons
                  name="right"
                  type="AntDesign"
                  color="gray"
                  size={20}
                />
              </Pressable>
            );
          }}
        />
      ) : (
        <TrackSkeleton />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  artAlbumsCard: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: '2%',
    marginHorizontal: '1%',
    borderRadius: 10,
    backgroundColor: '#fff',
    paddingVertical: 7,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 2,
  },
  titleText: {
    color: '#000',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  text: {color: 'gray', fontSize: 12, textAlign: 'center'},
});

import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Button,
  TextInput,
  FlatList,
  TouchableOpacity,
  Pressable,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import axios from 'axios';
import {Header, SkeletonCard} from '@components';
import {apikey, API} from '@services';

export default Artists = ({navigation}) => {
  const [artists, setArtists] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const increasePage = () => {
    setIsLoading(false);
    setPageNumber(prev => prev + 1);
  };
  const goBack = () => navigation.goBack();
  const getArtists = () => {
    axios
      .get(API + '/artists', {
        params: {
          page: pageNumber,
          apikey,
        },
      })
      .then(res => {
        setArtists(prev => [...prev, ...res.data.result]);
        setIsLoading(true);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  useEffect(() => {
    getArtists();
  }, [pageNumber]);

  return (
    <View>
      <Header  title={'Artists'}  />
      {artists.length > 0 ? (
        <FlatList
          data={artists}
          numColumns={2}
          contentContainerStyle={{paddingBottom: 80}}
          style={{marginHorizontal: '2%', paddingTop: 10}}
          onEndReached={() => isLoading && increasePage()}
          onEndThreshold={0.5}
          keyExtractor={(item, index) => index}
          ListFooterComponent={() => (
            <ActivityIndicator size="large" color="#000" />
          )}
          renderItem={({item}) => {
            return (
              <Pressable
                onPress={() =>
                  navigation.navigate('ArtAlbums', {
                    artID: item.id_artist,
                  })
                }
                style={styles.card}>
                <FastImage source={{uri: item.cover}} style={{height: 200}} />
                <Text
                  style={{
                    marginVertical: 4,
                    textAlign: 'center',
                    color: '#000',
                  }}>
                  {item.artist + item.id_artist}
                </Text>
              </Pressable>
            );
          }}
        />
      ) : (
        <SkeletonCard />
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  card: {
    borderRadius: 10,
    marginHorizontal: '1%',
    marginBottom: 10,
    width: '50%',
    overflow: 'hidden',
    backgroundColor: '#fff',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 3,
  },
});

import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  ScrollView,
  Dimensions,
  Pressable,
  StyleSheet,
} from 'react-native';
import axios from 'axios';
import {TrackSkeleton} from '@components';
import {AppIcons} from '@styles';
import FastImage from 'react-native-fast-image';
import {apikey, API} from '@services';

function Track({navigation, route}) {
  const {artist, album, track, image} = route.params;
  const [lyrics, setlyrics] = useState([]);
  const [trackDetails, setTrackDetails] = useState({});
  const goBack = () => navigation.goBack();
  let URL = `${API}/artists/${artist}/albums/${album}/tracks/${track}/lyrics`;
  const getLyrics = () => {
    axios
      .get(URL, {
        params: {
          apikey,
        },
      })
      .then(res => {
        if (res.data.success == 1) {
          let {lyrics, ...lyricsData} = res.data.result;
          setlyrics(lyrics);
          setTrackDetails(res.data.result);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  useEffect(() => {
    getLyrics();
  }, []);

  return (
    <View
      style={{
        alignItems: 'center',
        width: Dimensions.get('screen').width,
        backgroundColor: '#fff',
      }}>
      {lyrics.length > 0 ? (
        <View style={{width: '100%', height: '100%'}}>
        <View style={[styles.img]}>
        <FastImage
          source={{uri: image}}
          resizeMode="cover"
          style={styles.img}
        />
      </View>
          <ScrollView style={{width: '100%', height: '100%'}}>
            <View
              style={{alignItems: 'center', width: '85%', alignSelf: 'center'}}>
              <Text style={styles.track}>{trackDetails.track}</Text>
              <Text style={styles.lyrics}>{trackDetails.lyrics}</Text>
              <Text style={[styles.artist]}>{trackDetails.artist}</Text>
            </View>
          </ScrollView>
          <Pressable
            onPress={() => goBack()}
            style={{
              position: 'absolute',
              paddingHorizontal: 5,
              paddingVertical: 5,
              marginRight: 3,
              top: 7,
              left: 7,
            }}>
            <AppIcons
              name={"arrowleft"}
              type="AntDesign"
              color="#fff"
              size={20}
              style={{}}
            />
          </Pressable>
        </View>
      ) : (
        <TrackSkeleton />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  img: {
    height: 330,
    width: '100%',
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
  },
  track: {
    fontSize: 22,
    marginVertical: 15,
    color: '#000',
  },
  lyrics: {
    fontSize: 15,
    color: 'gray',
    textAlign: 'center',
    marginBottom: 10,
  },
  artist: {
    fontSize: 17,
    marginVertical: 15,
    color: '#000',
    alignSelf: 'flex-end',
    marginVertical: 20,
  },
});

export default Track;

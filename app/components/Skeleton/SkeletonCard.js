import React from 'react'
import { Dimensions } from 'react-native'
import ContentLoader, { Rect, Circle, Path } from "react-content-loader/native"

const SkeletonCard = props => {

  const rows = 5
  const columns = 2
  const coverHeight = 150 
  const coverWidth =Dimensions.get("screen").width/2.2
  const padding = 8
  const speed = 1
  
  const coverHeightWithPadding = coverHeight + padding
  const coverWidthWithPadding = coverWidth + padding
  const initial = 35
  const covers = Array(columns * rows).fill(1)
  
  return(
    
    <ContentLoader
    speed={speed}
    width={columns * coverWidthWithPadding}
    height={rows * coverHeightWithPadding}
    primaryColor="#fff"
    secondaryColor="#343d4c"
    backgroundColor="#fff"
    {...props}
    style={{marginHorizontal:20}}
    >
    

      {covers.map((g, i) => {
        let vy = Math.floor(i / columns) * coverHeightWithPadding + initial
        let vx = (i * coverWidthWithPadding) % (columns * coverWidthWithPadding)
        return (
          <Rect
          key={i}
          x={vx}
          y={vy}
          rx="0"
          ry="0"
          width={coverWidth}
          height={coverHeight}
          />
          )
        })}
    </ContentLoader>
 )
}
 
 
 
 export default SkeletonCard
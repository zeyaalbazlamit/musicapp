import React from 'react';
import {StyleSheet, View, Text, Pressable} from 'react-native';
import {Colors, AppStyles, AppIcons} from '@styles';

const Header = ({goBack, style, title, back}) => {
  const styles = StyleSheet.create({
    container: {
      ...AppStyles.header,
      justifyContent: 'space-between',
      alignItems: 'center',
      ...style,
    },
    title: {
      fontSize: 15,
      fontWeight:"bold",
      color: '#fff',
      textAlign: 'center',
      flex: back ? 0 : 1,
    },
    icon:{
      paddingHorizontal: 5,
      paddingVertical: 2,
      marginRight: 3,
    }
  });

  return (
    <View style={styles.container}>
      {back && (
        <Pressable
          onPress={() => goBack()}
          style={styles.icon}>
          <AppIcons
            size={20}
            name={'arrowleft'}
            type="AntDesign"
            color="#fff"
          />
        </Pressable>
      )}
      <Text style={styles.title}>{title}</Text>
      <View />
    </View>
  );
};

export default Header;

import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  View,
  Image,
} from 'react-native';
import {AppStyles, Colors, Sizes, Languages} from '@styles';
import {Text} from '@component';
export default function NoInternet() {
  const {X, Y} = useSelector(state => state.dimintions);
  return (
    <View style={{backgroundColor: '#ebebeb', height: '100%', width: '100%'}}>
      <View
        style={{
          width: X,
          height: '100%',
          alignItems: 'center',
          alignSelf: 'center',
          backgroundColor: '#fff',
        }}>
        <Image
          source={require('../images/noWifi.png')}
          resizeMode="contain"
          style={{height: '25%', width: X / 1.5, marginTop: Y / 9}}
        />
      <Text bold style={[
            {fontSize: X / 10, color: 'gray', marginTop: Y / 80},
          ]}>
          {Languages.oops}
        </Text>
      <Text bold style={[
            {fontSize: X / 28, color: 'gray', marginTop: 5},
          ]}>
          {Languages.noInternetConnectionfound}
        </Text>
        <Text bold style={[ {fontSize: X / 28, color: 'gray'}]}>
          {Languages.checkyourconnection}
        </Text>
      </View>
    </View>
  );
}

import React from 'react';
import {Text, StyleSheet, Dimensions, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {AppIcons} from '@styles';
import {
  Lyrics,
  ArtAlbums,
  LogIn,
  Artists,
  Albums,
  Search,
  Tracks,
  Splash,
} from '@screens';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const BottomTabs = () => {
  return (
    <Tab.Navigator screenOptions={{headerShown: false, keyboardHidesTabBar: true,
    }} 
    
    >
      <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarIcon: props => (
            <AppIcons
              {...props}
              type="AntDesign"
              name="search1"
              size={props.focused ? 28 : 22}
              color={props.focused ? '#9c27b0' : 'gray'}
            />
          ),
          tabBarLabel: props => (
            <Text
              style={{color: props.focused ? '#9c27b0' : 'gray', fontSize: 12}}>
              Search
            </Text>
          ),
        }}
      />
     
      <Tab.Screen
        name="Artists"
        component={Artists}
        options={{
          tabBarIcon: props => (
            <AppIcons
              {...props}
              name="user"
              type="Entypo"
              size={props.focused ? 28 : 22}
              color={props.focused ? '#9c27b0' : 'gray'}
            />
          ),
          tabBarLabel: props => (
            <Text
              style={{color: props.focused ? '#9c27b0' : 'gray', fontSize: 12}}>
              Artists
            </Text>
          ),
        }}
      />
      <Tab.Screen
        name="Albums"
        component={Albums}
        options={{
          tabBarIcon: props => (
            <AppIcons
              {...props}
              type="MaterialCommunityIcons"
              name={props.focused ? 'music-box-multiple' : 'music-box'}
              size={props.focused ? 28 : 22}
              color={props.focused ? '#9c27b0' : 'gray'}
            />
          ),
          tabBarLabel: props => (
            <Text
              style={{color: props.focused ? '#9c27b0' : 'gray', fontSize: 12}}>
              Albums
            </Text>
          ),
        }}
      />
    <Tab.Screen
        name="Tracks"
        component={Tracks}
        options={{
          tabBarIcon: props => (
            <AppIcons
              {...props}
              name={props.focused ? 'musical-notes' : 'musical-notes-outline'}
              type="Ionicons"
              size={props.focused ? 28 : 22}
              color={props.focused ? '#9c27b0' : 'gray'}
            />
          ),
          tabBarLabel: props => (
            <Text
              style={{color: props.focused ? '#9c27b0' : 'gray', fontSize: 12}}>
              Tracks
            </Text>
          ),
        }}
      />
  
    </Tab.Navigator>
  );
};

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="BottomTabs" component={BottomTabs} />
        <Stack.Screen name="Lyrics" component={Lyrics} />
        <Stack.Screen name="ArtAlbums" component={ArtAlbums} />
        <Stack.Screen name="LogIn" component={LogIn} />
        <Stack.Screen name="Tracks" component={Tracks} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default AppNavigator;

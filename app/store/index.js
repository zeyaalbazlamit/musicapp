import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk';
import userProfile from './reducers/userProfile';
import netWorkState from './reducers/netWorkState';

const Reducers = {userProfile,netWorkState}
const Store = createStore(combineReducers(Reducers), applyMiddleware(thunk));

export default Store;

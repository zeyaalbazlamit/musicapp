const initialState = {
  isOnline: true,
};

const netWorkState = (state = initialState, action) => {
  switch (action.type) {
    case 'SWITCH':
      console.log('Is connected?  ', action.isOnline);
      return {
        isOnline: action.isOnline,
      };
    default:
      return state;
  }
};

export default netWorkState;

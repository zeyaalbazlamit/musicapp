
export const setIsOnline = (isOnline) => {
    return {
      type: "SWITCH",
      isOnline
    };
  };
  
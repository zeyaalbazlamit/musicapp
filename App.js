import React from 'react';
import {SafeAreaView, StyleSheet,View} from 'react-native';
import {Provider} from 'react-redux';
import Store from './app/store';
import Navigation from '@navigation';
import Splash from './app/screens/Splash';

const App = () => {
  return (
    <Provider store={Store}>
      <SafeAreaView>
        <View
          style={{
            height: '100%',
          }}>
            <Navigation />  
        </View>
      </SafeAreaView>
    </Provider>
  );
};

const styles = StyleSheet.create({});

export default App;
